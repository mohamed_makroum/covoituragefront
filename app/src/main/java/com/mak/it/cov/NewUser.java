package com.mak.it.cov;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

public class NewUser extends AppCompatActivity {

    EditText etNom, etPhone, etdateN, etEmail;
    Spinner u_spinner;
    CircleImageView imageView;
    private static final int PICK_IMAGE = 100;
    Button button;

    private DatePickerDialog.OnDateSetListener mDateSetListener;

    private String url = LINKS.AJOUTER_USER;

    String idFirebase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_user);

        u_spinner = (Spinner) findViewById(R.id.spinner);
        etNom = (EditText) findViewById(R.id.et_nom);
        etPhone = (EditText) findViewById(R.id.et_telephone);
        etEmail = (EditText) findViewById(R.id.et_email);
        etdateN = (EditText) findViewById(R.id.et_dateN); //etdateN.setEd ;etdateN.setClickable(true);
        imageView = findViewById(R.id.profile_image);
        button = findViewById(R.id.btnEnregister);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //photoProfil();
            }
        });


        etdateN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        NewUser.this,
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        mDateSetListener,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setTitle("Date de naissance");
                dialog.show();

            }
        });

        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                //Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                String date = year + "-" + month + "-" + day;
                etdateN.setText(date);
            }
        };

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
            // Name, email address, and profile photo Url
            String phone = user.getPhoneNumber();
            etPhone.setText(phone);
            idFirebase = user.getUid();
        }

        //Spinner
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.Gender_array, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        u_spinner.setAdapter(adapter);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ajouterNewUser();
            }
        });
    }

    private void ajouterNewUser() {

//        String nn = etNom.getText().toString();
//        String mm = etEmail.getText().toString().trim();
//        String tt = etPhone.getText().toString().trim();
//        String dd = etdateN.getText().toString().trim();
//        String gg = u_spinner.getSelectedItem().toString();
//
//        Toast.makeText(NewUser.this, nn + " " + tt + " " + dd + " " + gg + " " + mm, Toast.LENGTH_LONG).show();

        RequestQueue queue = Volley.newRequestQueue(NewUser.this);

        StringRequest requestCheckUser = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.equals("success")) {
                    Intent intent = new Intent(NewUser.this, MenuActivity.class);
                    startActivity(intent);
                    finish();
                    Log.d("++++ myMessage reponse if", response);
                } else {
                    Toast.makeText(NewUser.this, "Quelque chose ne vas pas pd l'ajout", Toast.LENGTH_LONG).show();
                    Log.d("++++ myMessage reponse else", response);
                }

                Log.d("++++myMessage reponse", response);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(NewUser.this, error.toString(), Toast.LENGTH_LONG).show();
                Log.d("++++myMessage error : ", error.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("nom", etNom.getText().toString());
                params.put("idFirebase", idFirebase);
                params.put("dateN", etdateN.getText().toString());
                params.put("phone", etPhone.getText().toString().trim());
                params.put("gender", u_spinner.getSelectedItem().toString());
                params.put("email", etEmail.getText().toString().trim());

                return params;
            }
        };

        queue.add(requestCheckUser);


    }

    public void logOutNU(View view) {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(NewUser.this, MainActivity.class);
        startActivity(intent);
    }


}
