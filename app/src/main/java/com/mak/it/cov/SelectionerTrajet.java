package com.mak.it.cov;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mak.it.cov.DataSet.User;
import com.michaelmuenzer.android.scrollablennumberpicker.ScrollableNumberPicker;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SelectionerTrajet extends AppCompatActivity {

    private String url_CondInfo;
    private User user;

    private static final String TAG = "+++++++++ SelectionTrajet Activity ";
    private static final String[] CALL_PERMISSIONS = {Manifest.permission.CALL_PHONE};

    String e_id, e_dateDepart, e_heureDepart, e_dateArrivee, e_heureArrivee, e_cout, e_villeDepart, e_villeArrivee, e_bagage, e_nbrPlace;

    private String sr_idConducteur, sr_idFirebase, sr_nom, sr_gender, sr_ageConducteur, sr_telephone, sr_email;

    TextView tvST_dateDepart, tvST_heureDepart, tvST_dateArrivee, tvST_heureArrivee, tvST_cout, tvST_villeDepart, tvST_villeArrivee, tvST_bagage;
    TextView Infoc_nom, Infoc_age, Infoc_genre;
    EditText etST_remarque;
    ScrollableNumberPicker numberPicker;

    private String idFirebase;
    private String idFirebaseConducteur;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_selectioner_trajet);
        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            // Name, email address, and profile photo Url
            String phone = user.getPhoneNumber();

            idFirebase = user.getUid();
        }

        verifyPermissions();

        Intent i = getIntent();
        e_id = i.getStringExtra("ex_id");
        e_dateDepart = i.getStringExtra("ex_DateDepart");
        e_heureDepart = i.getStringExtra("ex_HeureDepart");
        e_dateArrivee = i.getStringExtra("ex_DateArrivee");
        e_heureArrivee = i.getStringExtra("ex_HeureArrivee");
        e_cout = i.getStringExtra("ex_Cout");
        e_villeDepart = i.getStringExtra("ex_VilleDepart");
        e_villeArrivee = i.getStringExtra("ex_VilleArrivee");
        e_bagage = i.getStringExtra("ex_Bagage");

        url_CondInfo = LINKS.INFO_CONDUCTEUR_BY_ID_TRAJET + "idTrajet=" + e_id;
        ConducteurInfo();

        Log.d("++++++++++++ MY LOG id ::  ", e_id);
        Log.d("+++++++++++ MY LOG url :: ", url_CondInfo);

        Infoc_nom = findViewById(R.id.tvCnom);
        Infoc_age = findViewById(R.id.tvCage);
        Infoc_genre = findViewById(R.id.tvCgenre);

        tvST_dateDepart = findViewById(R.id.tvSTdd);
        tvST_heureDepart = findViewById(R.id.tvSThd);
        tvST_dateArrivee = findViewById(R.id.tvSTda);
        tvST_heureArrivee = findViewById(R.id.tvSTha);
        tvST_cout = findViewById(R.id.tvSTCout);
        tvST_villeDepart = findViewById(R.id.tvSTvd);
        tvST_villeArrivee = findViewById(R.id.tvSTva);
        tvST_bagage = findViewById(R.id.tvSTba);
        numberPicker = findViewById(R.id.STnumber_picker_Place);
        etST_remarque = findViewById(R.id.etTSaqc);

        tvST_dateDepart.setText(e_dateDepart);
        tvST_heureDepart.setText(e_heureDepart);
        tvST_dateArrivee.setText(e_dateArrivee);
        tvST_heureArrivee.setText(e_heureArrivee);
        tvST_cout.setText(e_cout + " Dhs");
        tvST_villeDepart.setText(e_villeDepart);
        tvST_villeArrivee.setText(e_villeArrivee);
        tvST_bagage.setText(e_bagage + " bagage");

        if (e_bagage.equals("Sans")) {
            numberPicker.setValue(0);
            numberPicker.setVisibility(View.INVISIBLE);
        }

        findViewById(R.id.btnPhoneCall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + sr_telephone));
                startActivity(callIntent);
            }
        });
        findViewById(R.id.btnwhatsup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://api.whatsapp.com/send?phone=" + sr_telephone;
                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });

        findViewById(R.id.btnTS).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//
//                if (idFirebaseConducteur.equals(idFirebase)){
//                    Toast.makeText(SelectionerTrajet.this,"Tu ne peux pas ",Toast.LENGTH_LONG).show();
//                }
//
                DemandeCovoiturage();
            }
        });


    }


    void ConducteurInfo() {

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url_CondInfo, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);

                        idFirebaseConducteur = obj.getString("idFirebase");

                        sr_idConducteur = obj.getString("id");
                        sr_idFirebase = obj.getString("idFirebase");
                        sr_nom = obj.getString("nom");
                        sr_gender = obj.getString("gender");
                        sr_ageConducteur = obj.getString("ageConducteur");
                        sr_telephone = obj.getString("telephone");
                        sr_email = obj.getString("email");

                        Infoc_nom.setText(sr_nom);
                        Infoc_age.setText(sr_ageConducteur + " ans");
                        Infoc_genre.setText(sr_gender);

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(SelectionerTrajet.this, e.toString(), Toast.LENGTH_LONG).show();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder add = new AlertDialog.Builder(SelectionerTrajet.this);
                add.setMessage(error.getMessage()).setCancelable(true);
                AlertDialog alert = add.create();
                alert.setTitle("Error!!!");
                alert.show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(SelectionerTrajet.this);
        requestQueue.add(jsonArrayRequest);
    }

    private void verifyPermissions() {
        Log.d(TAG, "verifyPermissions: Checking Permissions.");

        int permissionCallPhone = ActivityCompat.checkSelfPermission(SelectionerTrajet.this, Manifest.permission.CALL_PHONE);

        int permissionExternalMemory = ActivityCompat.checkSelfPermission(SelectionerTrajet.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCallPhone != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    SelectionerTrajet.this,
                    CALL_PERMISSIONS,
                    1
            );
        }
    }


    private void DemandeCovoiturage() {

        RequestQueue queue = Volley.newRequestQueue(SelectionerTrajet.this);

        StringRequest requestCheckUser = new StringRequest(Request.Method.POST, LINKS.DEMANDE_COVOITURAGE, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                if (response.equals("success")) {

                    Log.d("++++ myMessage reponse if", response);

                    Intent intent = new Intent(SelectionerTrajet.this,MenuActivity.class);
                    startActivity(intent);
                    Toast.makeText(SelectionerTrajet.this, "Demande de covoiturage envoyer", Toast.LENGTH_LONG).show();

                }else if (response.equals("rowExist")){
                    Toast.makeText(SelectionerTrajet.this, "Vous avec déja envoyer une demande !", Toast.LENGTH_LONG).show();

                }else {
                    Toast.makeText(SelectionerTrajet.this, "Quelque chose ne vas pas pd l'ajout", Toast.LENGTH_LONG).show();
                    Log.d("++++ myMessage reponse else", response);
                }

                Log.d("++++myMessage reponse", response);


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(SelectionerTrajet.this, error.toString(), Toast.LENGTH_LONG).show();
                Log.d("++++myMessage error : ", error.toString());

            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                String nbr_bagage = String.valueOf(numberPicker.getValue());
                String remarque = etST_remarque.getText().toString();



                params.put("idProposition", e_id);
                params.put("idPassagerFirebase", idFirebase);
                params.put("nbrBagage", nbr_bagage);
                params.put("remarque", remarque);
                return params;


            }
        };

        queue.add(requestCheckUser);


    }


}


