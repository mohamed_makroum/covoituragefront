package com.mak.it.cov;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import static com.mak.it.cov.LINKS.INFO_DEMANDE_COV;

public class ConfirmDemandeCov extends AppCompatActivity {

    private static final String TAG = "+++++++++ ConfirmDemandeCov Activity ";
    private static final String[] CALL_PERMISSIONS = {Manifest.permission.CALL_PHONE};

    private String cdc_nom, cdc_gender, cdc_age, cdc_telephone, cdc_villeDepart, cdc_villeArrivee, cdc_nbrBagage, cdc_remarque;
    private String idDemandeCov;

    TextView tv_cdc_nom, tv_cdc_sexe, tv_cdc_age, tv_cdc_villeD, tv_cdc_villeA, tv_cdc_nbrBagage, tv_cdc_remarque;
    ImageButton btn_cdc_whatsup, IDb_cdc_PhoneCall;
    RadioButton radio1,radio0;
    RadioGroup radioGroupValidation;
    Button btn_cdc_confirmer;
    RadioButton radioButton;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_demande_cov);

        verifyPermissions();

        Intent intent = getIntent();
        idDemandeCov = intent.getStringExtra("idDemandeCov");

        tv_cdc_nom = (TextView) findViewById(R.id.tvIDnom);
        tv_cdc_sexe = (TextView) findViewById(R.id.tvIDsexe);
        tv_cdc_age = (TextView) findViewById(R.id.tvIDage);
        tv_cdc_villeD = (TextView) findViewById(R.id.tvIDvd);
        tv_cdc_villeA = (TextView) findViewById(R.id.tvIDva);
        tv_cdc_nbrBagage = (TextView) findViewById(R.id.tvIDba);
        tv_cdc_remarque = (TextView) findViewById(R.id.tvIDremarque);
        btn_cdc_whatsup = (ImageButton) findViewById(R.id.btnIDwhatsup);
        IDb_cdc_PhoneCall = (ImageButton) findViewById(R.id.btnIDPhoneCall);
        btn_cdc_confirmer = (Button) findViewById(R.id.btnIDconfirm);
        radioGroupValidation = (RadioGroup) findViewById(R.id.groupRadioValidation);

        PassagerInfo();

        btn_cdc_confirmer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            // get selected radio button from radioGroup
                int selectedId = radioGroupValidation.getCheckedRadioButtonId();

                // find the radiobutton by returned id
                radioButton = (RadioButton) findViewById(selectedId);

                ConfirmDC(radioButton.getText().toString());
            }
        });

        findViewById(R.id.btnIDPhoneCall).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + cdc_telephone));
                startActivity(callIntent);
            }
        });
        findViewById(R.id.btnIDwhatsup).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = "https://api.whatsapp.com/send?phone=" + cdc_telephone;
                Log.e("+++++++++++ whatssApp telephone", cdc_telephone);

                Intent i = new Intent(Intent.ACTION_VIEW);
                i.setData(Uri.parse(url));
                startActivity(i);
            }
        });


    }

    void PassagerInfo() {

        String url_DemandeInfoCov = LINKS.INFO_DEMANDE_COV + "idDemandeCov=" + idDemandeCov;
        Log.e("url_DemandeInfoCov", url_DemandeInfoCov);
        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url_DemandeInfoCov, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);

                        cdc_nom = obj.getString("nom");
                        cdc_gender = obj.getString("gender");
                        cdc_age = obj.getString("age");
                        cdc_telephone = obj.getString("telephone");
                        cdc_villeDepart = obj.getString("villeDepart");
                        cdc_villeArrivee = obj.getString("villeArrivee");
                        cdc_nbrBagage = obj.getString("nbrBagage");
                        cdc_remarque = obj.getString("remarque");

                        tv_cdc_nom.setText(cdc_nom);
                        tv_cdc_sexe.setText(cdc_gender);
                        tv_cdc_age.setText(cdc_age);
                        tv_cdc_villeD.setText(cdc_villeDepart);
                        tv_cdc_villeA.setText(cdc_villeArrivee);
                        if (cdc_nbrBagage == "0") {
                            tv_cdc_nbrBagage.setText("Aucun Bagage");
                        } else {
                            tv_cdc_nbrBagage.setText("Nbr bagage : " + cdc_nbrBagage);
                        }
                        if (cdc_remarque == "") {
                            tv_cdc_remarque.setText("Aucune remarque.");
                        } else {
                            tv_cdc_remarque.setText(cdc_remarque + ".");
                        }


                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(ConfirmDemandeCov.this, e.toString(), Toast.LENGTH_LONG).show();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder add = new AlertDialog.Builder(ConfirmDemandeCov.this);
                add.setMessage(error.getMessage()).setCancelable(true);
                AlertDialog alert = add.create();
                alert.setTitle("Error!!!");
                alert.show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(ConfirmDemandeCov.this);
        requestQueue.add(jsonArrayRequest);
    }

    public void ConfirmDC(final String validation) {

        Log.d("+++++++++++ validation +++++   ", validation);


        String url = LINKS.CONFIRMATION_DEMANDE_COV ;

        StringRequest requestAddProposition = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("+++++++++++MY LOG volley response +++++   ", response);

                if (response.equals("success")) {
                    Toast.makeText(ConfirmDemandeCov.this, "Demande "+ validation, Toast.LENGTH_LONG).show();

                    Intent intent = new Intent(ConfirmDemandeCov.this, MenuActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);

                } else {
                    Toast.makeText(ConfirmDemandeCov.this, "Erreur confirmation demande covoiturage ", Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("+++++++++++MY LOG volley error +++++   ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("idDemandeCov", idDemandeCov);
                params.put("validation", validation);

                return params;
            }
        };

        RequestQueue queue = Volley.newRequestQueue(this);
        queue.add(requestAddProposition);

    }

    private void verifyPermissions() {
        Log.d(TAG, "verifyPermissions: Checking Permissions.");

        int permissionCallPhone = ActivityCompat.checkSelfPermission(ConfirmDemandeCov.this, Manifest.permission.CALL_PHONE);

        int permissionExternalMemory = ActivityCompat.checkSelfPermission(ConfirmDemandeCov.this, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCallPhone != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    ConfirmDemandeCov.this,
                    CALL_PERMISSIONS,
                    1
            );
        }
    }

}
