package com.mak.it.cov.ListeAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mak.it.cov.DataSet.Proposition;

import java.util.ArrayList;

public class ListeAdapterListePropositions extends ArrayAdapter<Proposition> {

    private Context con;
    private int res;
    private ArrayList<Proposition> arl;


    public ListeAdapterListePropositions(Context context, int resource, ArrayList<Proposition> objects) {
        super(context, resource, objects);
        this.con = context;
        this.res = resource;
        this.arl = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = ((Activity) con).getLayoutInflater();
        View view = inflater.inflate(res, parent, false);

        TextView tvAdapt_dateDepart = (TextView) view.findViewById(com.mak.it.cov.R.id.tvSdd);
        TextView tvAdapt_id = (TextView) view.findViewById(com.mak.it.cov.R.id.tvSid);
        TextView tvAdapt_heureDepart = (TextView) view.findViewById(com.mak.it.cov.R.id.tvShd);
        TextView tvAdapt_dateArrivee = (TextView) view.findViewById(com.mak.it.cov.R.id.tvSda);
        TextView tvAdapt_heureArrivee = (TextView) view.findViewById(com.mak.it.cov.R.id.tvSha);
        TextView tvAdapt_cout = (TextView) view.findViewById(com.mak.it.cov.R.id.tvScout);
        TextView vtvAdapt_villeDepart = (TextView) view.findViewById(com.mak.it.cov.R.id.tvSvd);
        TextView tvAdapt_villeArrivee = (TextView) view.findViewById(com.mak.it.cov.R.id.tvSva);
        TextView tvAdapt_bagage = (TextView) view.findViewById(com.mak.it.cov.R.id.tvSb);
        TextView tvAdapt_nbrPlace = (TextView) view.findViewById(com.mak.it.cov.R.id.tvSpd);



        Proposition dataModelArrayList = arl.get(position);

        tvAdapt_id.setText(""+dataModelArrayList.getId());
        tvAdapt_dateDepart.setText(""+dataModelArrayList.getDateDepart());
        tvAdapt_heureDepart.setText(""+dataModelArrayList.getHeureDepart());
        tvAdapt_dateArrivee.setText(""+dataModelArrayList.getDateArrivee());
        tvAdapt_heureArrivee.setText(""+dataModelArrayList.getHeureArrivee());
        tvAdapt_cout.setText(dataModelArrayList.getCout()+" Dhs");
        vtvAdapt_villeDepart.setText(""+dataModelArrayList.getVilleDepart());
        tvAdapt_villeArrivee.setText(""+dataModelArrayList.getVilleArrivee());
        tvAdapt_bagage.setText(dataModelArrayList.getBagage()+" bagage");
        tvAdapt_nbrPlace.setText(""+dataModelArrayList.getNbrPlace());




        return view;

    }
}
