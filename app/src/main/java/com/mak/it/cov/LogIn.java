package com.mak.it.cov;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class LogIn extends AppCompatActivity {

    EditText et_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        et_phone = findViewById(R.id.editTextMobile);
    }

    public void sendCode(View view) {

        String mobile = et_phone.getText().toString().trim();

        if(mobile.isEmpty() || mobile.length() < 10){
            et_phone.setError("Enter a valid mobile");
            et_phone.requestFocus();
            return;
        }else{
            Intent intent = new Intent(LogIn.this,PhoneConfirmation.class);
            intent.putExtra("mobile", mobile);
            startActivity(intent);
        }





    }
}
