package com.mak.it.cov.DataSet;

public class DataSetUserVoyagesEnCours {

    String idProposition, dateDepart, villeDepart, villeArrivee, nbrDemande, dayLeft;

    public String getIdProposition() {
        return idProposition;
    }

    public void setIdProposition(String idProposition) {
        this.idProposition = idProposition;
    }

    public String getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(String dateDepart) {
        this.dateDepart = dateDepart;
    }

    public String getVilleDepart() {
        return villeDepart;
    }

    public void setVilleDepart(String villeDepart) {
        this.villeDepart = villeDepart;
    }

    public String getVilleArrivee() {
        return villeArrivee;
    }

    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    public String getNbrDemande() {
        return nbrDemande;
    }

    public void setNbrDemande(String nbrDemande) {
        this.nbrDemande = nbrDemande;
    }

    public String getDayLeft() {
        return dayLeft;
    }

    public void setDayLeft(String dayLeft) {
        this.dayLeft = dayLeft;
    }
}