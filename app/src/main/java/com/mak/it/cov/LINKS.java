package com.mak.it.cov;

public class LINKS {

    private static final String URL = "http://192.168.1.4/CoVoiturage/";

//    private static final String URL = "http://192.168.1.4/DUGLM/CoVoiturage/";

    public static final String CHECK_IF_USER_EXIST = URL + "checkUserExist.php?phone=";

    public static final String AJOUTER_USER = URL + "ajouterUser.php";

    public static final String AJOUTER_PROPOSITION = URL + "ajouterProposition.php";

    public static final String AFFICHER_PROPOSITIONS = URL + "listeAllTrajets.php";

    public static final String AFFICHER_PROPOSITION_PAR_VILLE = URL + "listeTrajetsParVille.php?";

    public static final String INFO_CONDUCTEUR_BY_ID_TRAJET = URL + "infoConducteurParIdTrajet.php?";

    public static final String DEMANDE_COVOITURAGE = URL + "demandeCovoiturage.php?";

    public static final String USER_DEMANDE_COVOITURAGE = URL + "userDemandeCov.php?";

    public static final String USER_VOYAGES_EN_COURS = URL + "userVoyagesEnCours.php?";

    public static final String USER_VOYAGES_TERMINER = URL + "userVoyages.php?";

//    public static final String USER_MES_DEMANDE_COVOITURAGE = URL + "userMesDemandeCov.php?";

    public static final String INFO_DEMANDE_COV = URL + "infoDemandeCov.php?";

    public static final String CONFIRMATION_DEMANDE_COV = URL + "confirmationDemandeCov.php";



}
