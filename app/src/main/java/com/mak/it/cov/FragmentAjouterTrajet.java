package com.mak.it.cov;


import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.support.v7.widget.AppCompatTextView;
import android.widget.Spinner;
import android.widget.TimePicker;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.michaelmuenzer.android.scrollablennumberpicker.ScrollableNumberPicker;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentAjouterTrajet extends Fragment {

    FirebaseUser user;
    private String idFirebase;

    EditText ETdateDepart, ETdateArrivee, ETheureDepart, ETheureArrivee, ETcout;
    AutoCompleteTextView ETvilleD, ETvilleA;
    ScrollableNumberPicker SNPplace;
    Spinner Sbagage;
    Button btnProposer;

    private DatePickerDialog.OnDateSetListener DateListenerDepart, DateListenerArrivee;
    private TimePickerDialog timePicker;

    private String[] Ville_array;
    private Resources res;
    private int length;

    private String url = LINKS.AJOUTER_PROPOSITION;


    public FragmentAjouterTrajet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_ajouter_trajet, container, false);

        user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            idFirebase = user.getUid();
        } else {
            Intent intent = new Intent(getContext(), MainActivity.class);
            startActivity(intent);
        }

        //get ville array to check
        res = getResources();
        Ville_array = res.getStringArray(R.array.Ville_array);
        length = Ville_array.length;


        //initialisation component
        ETdateDepart = (EditText) view.findViewById(R.id.et_dateDepart);
        ETdateArrivee = (EditText) view.findViewById(R.id.et_dateArrivee);
        ETheureDepart = (EditText) view.findViewById(R.id.et_heureDepart);
        ETheureArrivee = (EditText) view.findViewById(R.id.et_heureArrivee);
        ETcout = (EditText) view.findViewById(R.id.et_cout);

        ETvilleD = view.findViewById(R.id.actv_ville_depart);
        ETvilleA = view.findViewById(R.id.actv_ville_arrivee);

        SNPplace = view.findViewById(R.id.number_picker_Place);

        Sbagage = view.findViewById(R.id.spinner_Bagage);

        btnProposer = view.findViewById(R.id.btnProposer);

        //Auto AutoComplete VILLES
        String[] villes = getResources().getStringArray(R.array.Ville_array);
        ArrayAdapter<String> adapterVille = new ArrayAdapter<String>(getActivity().getApplicationContext(), android.R.layout.simple_list_item_1, villes);
        ETvilleD.setAdapter(adapterVille);
        ETvilleA.setAdapter(adapterVille);

        /////////////////DATE PICKER
        // DATE DEPART
        ETdateDepart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getContext(),
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        DateListenerDepart,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setTitle("Date de départ");
                dialog.show();

            }
        });

        DateListenerDepart = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                //Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                String date = year + "-" + month + "-" + day;
                ETdateDepart.setText(date);
            }
        };

        // DATE ARRIVEE
        ETdateArrivee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar cal = Calendar.getInstance();
                int year = cal.get(Calendar.YEAR);
                int month = cal.get(Calendar.MONTH);
                int day = cal.get(Calendar.DAY_OF_MONTH);

                DatePickerDialog dialog = new DatePickerDialog(
                        getContext(),
                        android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                        DateListenerArrivee,
                        year, month, day);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                dialog.setTitle("Date de départ");
                dialog.show();

            }
        });

        DateListenerArrivee = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
                month = month + 1;
                //Log.d(TAG, "onDateSet: mm/dd/yyy: " + month + "/" + day + "/" + year);

                String date = year + "-" + month + "-" + day;
                ETdateArrivee.setText(date);
            }
        };


        //HEURE DEPART
        ETheureDepart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ETheureDepart.setText(hourOfDay + ":" + minute);
                    }
                }, 0, 0, false);
                timePicker.show();
            }
        });

        //HEURE ARRIVEE
        ETheureArrivee.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                timePicker = new TimePickerDialog(getContext(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        ETheureArrivee.setText(hourOfDay + ":" + minute);
                    }
                }, 0, 0, false);
                timePicker.show();
            }
        });

        // BTN PROPOSER CLICK LISTNER
        btnProposer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String sNbrPlace = String.valueOf(SNPplace.getValue());
                String sDateDepart = ETdateDepart.getText().toString();
                String sDateArrivee = ETdateArrivee.getText().toString();
                String sHeureDepart = ETheureDepart.getText().toString();
                String sHeureArrivee = ETheureArrivee.getText().toString();
                String sCout = ETcout.getText().toString();
                String sVilleDepart = ETvilleD.getText().toString();
                String sVilleArrivee = ETvilleA.getText().toString();
                String sBagage = Sbagage.getSelectedItem().toString();

                //check si les villes sont dispo
                int idepart = checkIfCityDepartExist(sVilleDepart);
                int iarrivee = checkIfCityDepartExist(sVilleArrivee);

                if (iarrivee == 0) {
                    ETvilleA.setError("Ville valide est requise");
                }
                if (idepart == 0) {
                    ETvilleD.setError("Ville valide est requise");
                }
                if (idepart == 1 && iarrivee == 1) {

                    // CHECK IF NOT EMPTY
                    if (sDateDepart.equals("")) {
                        ETdateDepart.setError("Champ requis");
                    } else if (sDateArrivee.equals("")) {
                        ETdateArrivee.setError("Champ requis");
                    } else if (sHeureArrivee.equals("")) {
                        ETheureArrivee.setError("Champ requis");
                    } else if (sHeureDepart.equals("")) {
                        ETheureDepart.setText("Champ requis");
                    } else if (sCout.equals("")) {
                        ETcout.setError("Champ requis");
                    } else {
                        AjouterProposition(sVilleDepart, sVilleArrivee, sDateDepart, sDateArrivee, sHeureDepart, sHeureArrivee, sBagage, sCout, sNbrPlace);

                    }

                } else {
                    Toast.makeText(getContext(), "**checkcitydispo** Quelque chose ne vas pas ", Toast.LENGTH_LONG).show();
                }


            }
        });


        return view;
    }


    private void AjouterProposition(final String vd, final String va, final String dd, final String da, final String hd, final String ha, final String bag, final String co, final String nbrPlace) {

        Log.d("+++++++++++MY LOG   ", idFirebase + "\n" + dd + "\n" + da + "\n" + hd + "\n" + ha + "\n" + co + "\n" + vd + "\n" + va + "\n" + bag + "\n" + nbrPlace);

        RequestQueue queue = Volley.newRequestQueue(getContext());

        StringRequest requestAddProposition = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("+++++++++++MY LOG volley response +++++   ", response);

                if (response.equals("success")) {
                    Log.d("+++++++++++MY LOG volley response +++++   ", "YEAAAAH *_* " + response);
                    Toast.makeText(getContext(), "Proposition ajouter avec succes", Toast.LENGTH_LONG).show();
                    viderLesChampsProposition();
                } else {
                    Log.d("+++++++++++MY LOG volley response +++++   ", " NOOOO *o*  " + response);
                    Toast.makeText(getContext(), "Erreur lors de l'ajout de la proposition", Toast.LENGTH_LONG).show();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d("+++++++++++MY LOG volley error +++++   ", error.toString());
            }
        }) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();

                params.put("idFirebase", idFirebase);
                params.put("dateDepart", dd);
                params.put("dateArrivee", da);
                params.put("heureDepart", hd);
                params.put("heureArrivee", ha);
                params.put("cout", co);
                params.put("villeDepart", vd);
                params.put("villeArrivee", va);
                params.put("bagage", bag);
                params.put("nbrPlace", nbrPlace);

                return params;
            }
        };

        queue.add(requestAddProposition);

    }

    private int checkIfCityDepartExist(String d) {
        int r = 0;
        for (int i = 0; i < length; i++) {
            if (d.equals(Ville_array[i])) {
                r = 1;
            }
        }
        return r;
    }

    private int checkIfCityArriveeExist(String a) {
        int r = 0;
        for (int i = 0; i < length; i++) {
            if (a.equals(Ville_array[i])) {
                r = 1;
            }
        }
        return r;
    }

    public void logOut() {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(getContext(), MainActivity.class);
        startActivity(intent);
    }

    void viderLesChampsProposition() {

        SNPplace.setValue(1);
        ETdateDepart.setText("");
        ETdateArrivee.setText("");
        ETheureDepart.setText("");
        ETheureArrivee.setText("");
        ETcout.setText("");
        ETvilleD.setText("");
        ETvilleA.setText("");
        Sbagage.setSelection(0);
    }

}
