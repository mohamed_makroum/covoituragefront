package com.mak.it.cov.ListeAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;


import com.mak.it.cov.DataSet.DataSetDemandeCovoiturage;
import com.mak.it.cov.R;

import java.util.ArrayList;

public class ListeAdapterDemandeCov extends ArrayAdapter<DataSetDemandeCovoiturage> {

    private Context con;
    private int res;
    private ArrayList<DataSetDemandeCovoiturage> arl;


    public ListeAdapterDemandeCov(Context context, int resource, ArrayList<DataSetDemandeCovoiturage> objects) {
        super(context, resource, objects);

        this.con = context;
        this.res = resource;
        this.arl = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = ((Activity) con).getLayoutInflater();
        View view = inflater.inflate(res, parent, false);

        TextView tv_adc_vd = (TextView) view.findViewById(com.mak.it.cov.R.id.tv_adc_vd);
        TextView tv_adc_va = (TextView) view.findViewById(com.mak.it.cov.R.id.tv_adc_va);
        TextView tv_adc_dd = (TextView) view.findViewById(com.mak.it.cov.R.id.tv_adc_dd);
        TextView tv_adc_nom = (TextView) view.findViewById(com.mak.it.cov.R.id.tv_adc_nom);
        TextView tv_adc_validation = (TextView) view.findViewById(com.mak.it.cov.R.id.validation);

        DataSetDemandeCovoiturage demandeCovoiturage = arl.get(position);

        tv_adc_vd.setText(demandeCovoiturage.getVilleDepart());
        tv_adc_va.setText(demandeCovoiturage.getVilleArrivee());
        tv_adc_dd.setText(demandeCovoiturage.getDateDepart());
        tv_adc_nom.setText(demandeCovoiturage.getNom());
        tv_adc_validation.setText(demandeCovoiturage.getValidation());

        if (demandeCovoiturage.getValidation().equals("Refusé")){
            tv_adc_validation.setBackgroundResource(R.color.colorRed);
        }else if (demandeCovoiturage.getValidation().equals("Accepter")){
            tv_adc_validation.setBackgroundResource(R.color.colorGreen);
        } else {
            tv_adc_validation.setBackgroundResource(R.color.colorGrey);
        }

        return view;
    }
}
