package com.mak.it.cov;


import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

public class PagerAdapter extends FragmentStatePagerAdapter {

    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        this.mNumOfTabs = NumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {

        switch (position) {
            case 0:
                FragmentChercherTrajet tab1 = new FragmentChercherTrajet();
                return tab1;
            case 1:
                FragmentAjouterTrajet tab2 = new FragmentAjouterTrajet();
                return tab2;
            case 2:
                FragmentMesTrajets tab3 = new FragmentMesTrajets();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}