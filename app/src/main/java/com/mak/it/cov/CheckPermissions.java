package com.mak.it.cov;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;

public class CheckPermissions {
    Context context;
    private static final String TAG = "MainActivity";
    private static final String[] CALL_PERMISSIONS = {Manifest.permission.CALL_PHONE};


    private void verifyPermissions() {
        Log.d(TAG, "verifyPermissions: Checking Permissions.");

        int permissionCallPhone = ActivityCompat.checkSelfPermission(context, Manifest.permission.CALL_PHONE);

        int permissionExternalMemory = ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE);

        if (permissionCallPhone != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    (Activity) context,
                    CALL_PERMISSIONS,
                    1
            );
        }


    }
}
