package com.mak.it.cov;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mak.it.cov.DataSet.DataSetUserVoyagesEnCours;
import com.mak.it.cov.ListeAdapter.ListeAdapterDemandeCov;
import com.mak.it.cov.DataSet.DataSetDemandeCovoiturage;
import com.mak.it.cov.ListeAdapter.ListeAdapterUserVoyagesEnCours;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentMesTrajets extends Fragment {

    FirebaseUser user;
    String userIDfirebase;

    int lv1 = 0, lv2 = 0, lv3 = 0;

    private TextView tvdc, tvvv, tvlc;
    private ListView lvdc, lvvv, lvlc;

    SwipeRefreshLayout refreshLayout;

    private String URLuserDD = LINKS.USER_DEMANDE_COVOITURAGE;
    private String URLuserVC = LINKS.USER_VOYAGES_EN_COURS;
    private String URLuserVT = LINKS.USER_VOYAGES_TERMINER;

    ArrayList<DataSetDemandeCovoiturage> arrayListDemandeCov;
    ListeAdapterDemandeCov listeAdapterDemandeCov;
    DataSetDemandeCovoiturage dataSetDemandeCovoiturage;

    ArrayList<DataSetUserVoyagesEnCours> arrayListUserVoyages;
    ListeAdapterUserVoyagesEnCours listeAdapterUserVoyages;
    DataSetUserVoyagesEnCours dataSetUserVoyages;



    public FragmentMesTrajets() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mes_trajets, container, false);

        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            userIDfirebase = user.getUid();
        }

        refreshLayout = view.findViewById(R.id.refreshdc);

        tvdc = (TextView) view.findViewById(R.id.dc);
        tvvv = (TextView) view.findViewById(R.id.vv);
//        tvmd = (TextView) view.findViewById(R.id.md);
//
//
        lvdc = (ListView) view.findViewById(R.id.lv_dc);
        lvvv = (ListView) view.findViewById(R.id.lv_vv);
//        lvlc = (ListView) view.findViewById(R.id.lv_mc);


        AfficherDemandeCov();
        arrayListDemandeCov = new ArrayList<DataSetDemandeCovoiturage>();
        listeAdapterDemandeCov = new ListeAdapterDemandeCov(getContext(),R.layout.adapter_demande_covoiturage,arrayListDemandeCov);
        lvdc.setAdapter(listeAdapterDemandeCov);

        AfficherUserVoyagesEnCours();
        arrayListUserVoyages = new ArrayList<DataSetUserVoyagesEnCours>();
        listeAdapterUserVoyages = new ListeAdapterUserVoyagesEnCours(getContext(),R.layout.adapter_futur_voyages,arrayListUserVoyages);
        lvvv.setAdapter(listeAdapterUserVoyages);



        tvdc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (lv1 == 0) {
                    lv1 = 1;
                    lv2 = 0;
                    tvdc.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_remove, 0);
                    tvvv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add, 0);
                    tvvv.setBackgroundResource(R.drawable.liste_botom_radius);
                    lvdc.setVisibility(View.VISIBLE);
                    lvvv.setVisibility(View.GONE);
                } else if (lv1 == 1) {
                    lv1 = 0;
                    tvdc.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add, 0);
                    lvdc.setVisibility(View.GONE);
                }

//                Toast.makeText(getContext(), "LV DC :" + lv1+" MV:"+lv2, Toast.LENGTH_LONG).show();

            }
        });


        tvvv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (lv2 == 0) {
                    lv2 = 1;
                    lv1 = 0;
                    tvvv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_remove, 0);
                    tvdc.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add, 0);
                    tvvv.setBackgroundResource(R.drawable.liste_botom_radius);
                    lvvv.setVisibility(View.VISIBLE);
                    lvdc.setVisibility(View.GONE);

                    tvvv.setBackgroundResource(R.drawable.rectangle_darklight_blue_no_radius);
                } else if (lv2 == 1) {
                    lv2 = 0;
                    tvvv.setCompoundDrawablesWithIntrinsicBounds(0, 0, R.drawable.ic_add, 0);
                    tvvv.setBackgroundResource(R.drawable.liste_botom_radius);
                    lvvv.setVisibility(View.GONE);
                }

//                Toast.makeText(getContext(), "LV CD :" + lv1+" VV:"+lv2, Toast.LENGTH_LONG).show();

            }
        });

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                new Handler().postDelayed(new Runnable() {
                    public void run() {

                        arrayListUserVoyages.clear();
                        AfficherUserVoyagesEnCours();
                        listeAdapterUserVoyages.notifyDataSetChanged();

                        arrayListDemandeCov.clear();
                        AfficherDemandeCov();
                        listeAdapterDemandeCov.notifyDataSetChanged();

                        refreshLayout.setRefreshing(false);

                    }
                }, 2000);
            }
        });

        lvdc.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String idDemandeCov = arrayListDemandeCov.get(position).getIdDemande();

//                Toast.makeText(getContext(),idDemandeCov , Toast.LENGTH_LONG).show();

                Intent intent = new Intent(getContext(), ConfirmDemandeCov.class);
                intent.putExtra("idDemandeCov",idDemandeCov);
                startActivity(intent);

            }
        });



        return view;
    }

    private void AfficherDemandeCov() {

//        Log.e("FragmentMesTrajets ++++++++++++++", "AfficherDemandeCov ++IN++ 1");

        String url = URLuserDD + "idFirebase=" + userIDfirebase;
//        Log.e("FragmentMesTrajet  AfficherLesTrajetParUser url", url);


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
//                Log.e("FragmentMesTrajet ", response + "");

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        dataSetDemandeCovoiturage = new DataSetDemandeCovoiturage();

                        dataSetDemandeCovoiturage.setIdDemande(obj.getString("idDemande"));
                        dataSetDemandeCovoiturage.setIdProposition(obj.getString("idProposition"));
                        dataSetDemandeCovoiturage.setIdPassagerFirebase(obj.getString("idPassagerFirebase"));
                        dataSetDemandeCovoiturage.setDateDepart(obj.getString("dateDepart"));
                        dataSetDemandeCovoiturage.setHeureDepart(obj.getString("heureDepart"));
                        dataSetDemandeCovoiturage.setCout(obj.getString("cout"));
                        dataSetDemandeCovoiturage.setVilleDepart(obj.getString("villeDepart"));
                        dataSetDemandeCovoiturage.setVilleArrivee(obj.getString("villeArrivee"));
                        dataSetDemandeCovoiturage.setValidation(obj.getString("validation"));
                        dataSetDemandeCovoiturage.setNom(obj.getString("nom"));


                        arrayListDemandeCov.add(dataSetDemandeCovoiturage);
//                        Log.e("FragmentMesTrajets ++++++++++++++", "AfficherDemandeCov ++IN++ 2");

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), e.toString(), Toast.LENGTH_LONG).show();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder add = new AlertDialog.Builder(getContext());
                add.setMessage(error.getMessage()).setCancelable(true);
                AlertDialog alert = add.create();
                alert.setTitle("Error!!!");
                alert.show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(jsonArrayRequest);


    }


    private void AfficherUserVoyagesEnCours() {

        Log.e("FragmentMesTrajets ++++++++++++++", "AfficherUserVoyages ++IN++ 1");

        String url = URLuserVC + "idFirebase=" + userIDfirebase;
        Log.e("FragmentMesTrajet  AfficherUserVoyages url", url);


        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.e("FragmentMesTrajet  AfficherUserVoyages ", response + "");

                for (int i = 0; i < response.length(); i++) {
                    try {
                        JSONObject obj = response.getJSONObject(i);
                        dataSetUserVoyages = new DataSetUserVoyagesEnCours();

                        dataSetUserVoyages.setIdProposition(obj.getString("id"));
                        dataSetUserVoyages.setDateDepart(obj.getString("dateDepart"));
                        dataSetUserVoyages.setVilleDepart(obj.getString("villeDepart"));
                        dataSetUserVoyages.setVilleArrivee(obj.getString("villeArrivee"));
                        dataSetUserVoyages.setNbrDemande(obj.getString("nbrDemande"));
                        dataSetUserVoyages.setDayLeft(obj.getString("dayLeft"));


                        arrayListUserVoyages.add(dataSetUserVoyages);
                        Log.e("FragmentMesTrajets ++++++++++++++", "AfficherDemandeCov ++IN++ 2");

                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), e.toString(), Toast.LENGTH_LONG).show();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder add = new AlertDialog.Builder(getContext());
                add.setMessage(error.getMessage()).setCancelable(true);
                AlertDialog alert = add.create();
                alert.setTitle("Error!!!");
                alert.show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(jsonArrayRequest);


    }
//
//    public void refreshclick() {
//        refreshLayout.setRefreshing(false);
//
//    }
}