package com.mak.it.cov;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import java.util.HashMap;
import java.util.Map;

public class CheckIfUserExist extends AppCompatActivity {

    String phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ProgressBar progressBar = findViewById(R.id.progressBar);
        progressBar.setVisibility(View.VISIBLE);

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        if (user != null) {
            phone = user.getPhoneNumber();
        }


        RequestQueue queue = Volley.newRequestQueue(CheckIfUserExist.this);

        String url = LINKS.CHECK_IF_USER_EXIST+phone;
        Log.d("+++++++++++++++++++++ CheckIfUserExist Class ", " link: "+url);

        StringRequest requestCheckUser = new StringRequest(url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {

                Log.d("+++++++++++++++++++++ CheckIfUserExist Class ",response);

                if (response.equals("yes")) {
                    Intent intent = new Intent(CheckIfUserExist.this, MenuActivity.class);
                    startActivity(intent);
                    finish();
                } else if (response.equals("no")) {
                    Intent intent = new Intent(CheckIfUserExist.this, NewUser.class);
                    startActivity(intent);
                    finish();
                } else {
                    Toast.makeText(CheckIfUserExist.this, "Quelque chose ne vas pas", Toast.LENGTH_LONG).show();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(CheckIfUserExist.this, error.toString(), Toast.LENGTH_LONG).show();

            }
        }) ;

        queue.add(requestCheckUser);
    }


}
