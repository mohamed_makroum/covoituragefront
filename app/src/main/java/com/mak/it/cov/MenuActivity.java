package com.mak.it.cov;

import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MenuActivity extends AppCompatActivity {

//    TextView tvUserName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tab_layout);
        tabLayout.addTab(tabLayout.newTab().setText("Chercher trajet").setIcon(R.drawable.carsearch));
        tabLayout.addTab(tabLayout.newTab().setText("Ajouter trajet").setIcon(R.drawable.carrepair));
        tabLayout.addTab(tabLayout.newTab().setText("Mes trajets").setIcon(R.drawable.carliste));

//        tvUserName = findViewById(R.id.tvUserName);

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.pager);
        viewPager.setOffscreenPageLimit(3);
        final PagerAdapter adapter = new PagerAdapter
                (getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                viewPager.setCurrentItem(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

//        // hide auto star keyboard
//        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//        if (id == R.id.action_settings) {
//            return true;
//        }

        return super.onOptionsItemSelected(item);
    }

    public void LogOut(View view) {
        FirebaseAuth.getInstance().signOut();
        Intent intent = new Intent(MenuActivity.this,MainActivity.class);
        startActivity(intent);

    }

    public void MyProfil(View view) {

        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {
//            String phone = user.getPhoneNumber();
            String  idF = user.getUid();

            Toast.makeText(this,"Hello "+idF,Toast.LENGTH_LONG).show();
        }

    }

    void UserrInfo() {



//        String url_CondInfo="";
//        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url_CondInfo, new Response.Listener<JSONArray>() {
//            @Override
//            public void onResponse(JSONArray response) {
//
//                for (int i = 0; i < response.length(); i++) {
//                    try {
//                        JSONObject obj = response.getJSONObject(i);
//
//                        String user_name = obj.getString("nom");
//
//                        tvUserName.setText(user_name);
//
//                    } catch (JSONException e) {
//                        e.printStackTrace();
//                        Toast.makeText(MenuActivity.this, e.toString(), Toast.LENGTH_LONG).show();
//                    }
//                }
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                AlertDialog.Builder add = new AlertDialog.Builder(MenuActivity.this);
//                add.setMessage(error.getMessage()).setCancelable(true);
//                AlertDialog alert = add.create();
//                alert.setTitle("Error!!!");
//                alert.show();
//            }
//        });
//
//        RequestQueue requestQueue = Volley.newRequestQueue(MenuActivity.this);
//        requestQueue.add(jsonArrayRequest);
    }




}