package com.mak.it.cov.ListeAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mak.it.cov.DataSet.DataSetLMaisCovoiturage;

import java.util.ArrayList;

public class ListeAdapterUserMaisCov extends ArrayAdapter<DataSetLMaisCovoiturage> {

    private Context con;
    private int res;
    private ArrayList<DataSetLMaisCovoiturage> arl;


    public ListeAdapterUserMaisCov(Context context, int resource, ArrayList<DataSetLMaisCovoiturage> objects) {
        super(context, resource, objects);

        this.con = context;
        this.res = resource;
        this.arl = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = ((Activity) con).getLayoutInflater();
        View view = inflater.inflate(res, parent, false);

        TextView tv_alc_vd = (TextView) view.findViewById(com.mak.it.cov.R.id.addc_vd);
        TextView tv_alc_va = (TextView) view.findViewById(com.mak.it.cov.R.id.addc_va);
        TextView tv_alc_dd = (TextView) view.findViewById(com.mak.it.cov.R.id.addc_dd);
        TextView tv_alc_prix = (TextView) view.findViewById(com.mak.it.cov.R.id.addc_prix);
        TextView tv_alc_idCond = (TextView) view.findViewById(com.mak.it.cov.R.id.addc_conducteur);


        DataSetLMaisCovoiturage dataModelArrayList = arl.get(position);

        tv_alc_vd.setText("" + dataModelArrayList.getVilleDepart());
        tv_alc_va.setText("" + dataModelArrayList.getVilleArrivee());
        tv_alc_dd.setText("" + dataModelArrayList.getDateDepart());
        tv_alc_prix.setText("" + dataModelArrayList.getCout());
        tv_alc_idCond.setText("" + dataModelArrayList.getIdConducteur());


        return view;

    }
}
