package com.mak.it.cov;


import android.app.AlertDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.mak.it.cov.DataSet.Proposition;
import com.mak.it.cov.ListeAdapter.ListeAdapterListePropositions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


/**
 * A simple {@link Fragment} subclass.
 */
public class FragmentChercherTrajet extends Fragment {

    FirebaseUser user;
    String userIDfirebase;

    private String urlAfficherProposition;
    private String urlChercherByVille = LINKS.AFFICHER_PROPOSITION_PAR_VILLE;

    private Adapter adapter;
    private ArrayList<Proposition> arrayListProposition;
    private ListeAdapterListePropositions listeAdapter;
    private Proposition proposition;

    private EditText villeDepart, villeArrivee;

    SwipeRefreshLayout swipeRefresh;
    ListView listView;

    String SVDepart = "", SVArrivee = "";



    public FragmentChercherTrajet() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_chercher_trajet, container, false);

        user = FirebaseAuth.getInstance().getCurrentUser();
        if (user != null) {

            userIDfirebase = user.getUid();
        }
        urlAfficherProposition = LINKS.AFFICHER_PROPOSITIONS+"?idFirebase="+userIDfirebase;


        swipeRefresh = view.findViewById(R.id.refresh);
        listView = (ListView) view.findViewById(R.id.lv);

        Log.e("++++++++++++++++++++++++++ MY LOG : ", "##6");
        arrayListProposition = new ArrayList<Proposition>();
        Log.e("++++++++++++++++++++++++++ MY LOG : ", "##7");
        listeAdapter = new ListeAdapterListePropositions(getContext(), R.layout.adapter_list_proposition, arrayListProposition);
        Log.e("++++++++++++++++++++++++++ MY LOG : ", "##8");
        listView.setAdapter(listeAdapter);
        Log.e("++++++++++++++++++++++++++ MY LOG : ", "##9");
        afficherAllProposition();
        Log.e("++++++++++++++++++++++++++ MY LOG : ", "##10");

        villeDepart = (EditText) view.findViewById(R.id.etSvd);
        villeArrivee = (EditText) view.findViewById(R.id.etSva);



        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                arrayListProposition.clear();
                swipRefreshListe();
            }
        });


        view.findViewById(R.id.btnChercherTrajet).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AfficherLesTrajetParVille();
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String ex_id = arrayListProposition.get(position).getId();
                String ex_DateDepart = arrayListProposition.get(position).getDateDepart();
                String ex_HeureDepart = arrayListProposition.get(position).getHeureDepart();
                String ex_DateArrivee = arrayListProposition.get(position).getDateArrivee();
                String ex_HeureArrivee = arrayListProposition.get(position).getHeureArrivee();
                String ex_Cout = arrayListProposition.get(position).getCout();
                String ex_VilleDepart = arrayListProposition.get(position).getVilleDepart();
                String ex_VilleArrivee = arrayListProposition.get(position).getVilleArrivee();
                String ex_Bagage = arrayListProposition.get(position).getBagage();
                String ex_NbrPlace = arrayListProposition.get(position).getNbrPlace();

                Intent intent = new Intent(getContext(), SelectionerTrajet.class);
                intent.putExtra("ex_id", ex_id);
                intent.putExtra("ex_DateDepart", ex_DateDepart);
                intent.putExtra("ex_HeureDepart", ex_HeureDepart);
                intent.putExtra("ex_DateArrivee", ex_DateArrivee);
                intent.putExtra("ex_HeureArrivee", ex_HeureArrivee);
                intent.putExtra("ex_Cout", ex_Cout);
                intent.putExtra("ex_VilleDepart", ex_VilleDepart);
                intent.putExtra("ex_VilleArrivee", ex_VilleArrivee);
                intent.putExtra("ex_Bagage", ex_Bagage);
                intent.putExtra("ex_NbrPlace", ex_NbrPlace);
                startActivity(intent);


            }
        });


//        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        return view;
    }

    private void afficherAllProposition() {

        JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(urlAfficherProposition, new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {

                Log.e("++++++++++++++++++++++++++ MY LOG response: ", response.toString());
                for (int i = 0; i < response.length(); i++) {
                    try {
                        Log.e("++++++++++++++++++++++++++ MY LOG : ", "##1");
                        JSONObject obj = response.getJSONObject(i);
                        Log.e("++++++++++++++++++++++++++ MY LOG : ", "##2");
                        proposition = new Proposition();
                        Log.e("++++++++++++++++++++++++++ MY LOG : ", "##3");

                        proposition.setId(obj.getString("id"));
                        proposition.setDateDepart(obj.getString("dateDepart"));
                        proposition.setHeureDepart(obj.getString("heureDepart"));
                        proposition.setDateArrivee(obj.getString("dateArrivee"));
                        proposition.setHeureArrivee(obj.getString("heureArrivee"));
                        proposition.setCout(obj.getString("cout"));
                        proposition.setVilleDepart(obj.getString("villeDepart"));
                        proposition.setVilleArrivee(obj.getString("villeArrivee"));
                        proposition.setBagage(obj.getString("bagage"));
                        proposition.setNbrPlace(obj.getString("nbrPlace"));

                        Log.e("++++++++++++++++++++++++++ MY LOG : ", "##4");
                        arrayListProposition.add(proposition);

                        Log.e("++++++++++++++++++++++++++ MY LOG : ", "##5");
                    } catch (JSONException e) {
                        e.printStackTrace();
                        Toast.makeText(getContext(), e.toString(), Toast.LENGTH_LONG).show();
                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                AlertDialog.Builder add = new AlertDialog.Builder(getContext());
                add.setMessage(error.getMessage()).setCancelable(true);
                AlertDialog alert = add.create();
                alert.setTitle("Error!!!");
                alert.show();
            }
        });

        RequestQueue requestQueue = Volley.newRequestQueue(getContext());
        requestQueue.add(jsonArrayRequest);

    }

    private void AfficherLesTrajetParVille() {

        SVDepart = villeDepart.getText().toString();
        SVArrivee = villeArrivee.getText().toString();

        String url = urlChercherByVille + "villeDepart=" + SVDepart + "&villeArrivee=" + SVArrivee+"&idFirebase="+userIDfirebase;;
        Log.d("++++++++++++++++++++++++++ MY LOG : ", url);

        if (SVDepart.equals("")) {

            villeDepart.setError("Saisisez votre depart");

        } else if (SVArrivee.equals("")) {

            villeArrivee.setError("Saisisez votre destination");

        } else {

            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
                @Override
                public void onResponse(JSONArray response) {

                    for (int i = 0; i < response.length(); i++) {
                        try {
                            JSONObject obj = response.getJSONObject(i);
                            proposition = new Proposition();
                            proposition.setId(obj.getString("id"));
                            proposition.setDateDepart(obj.getString("dateDepart"));
                            proposition.setHeureDepart(obj.getString("heureDepart"));
                            proposition.setDateArrivee(obj.getString("dateArrivee"));
                            proposition.setHeureArrivee(obj.getString("heureArrivee"));
                            proposition.setCout(obj.getString("cout"));
                            proposition.setVilleDepart(obj.getString("villeDepart"));
                            proposition.setVilleArrivee(obj.getString("villeArrivee"));
                            proposition.setBagage(obj.getString("bagage"));
                            proposition.setNbrPlace(obj.getString("nbrPlace"));

                            arrayListProposition.clear();
                            arrayListProposition.add(proposition);
                            listeAdapter.notifyDataSetChanged();

                        } catch (JSONException e) {
                            e.printStackTrace();
                            Toast.makeText(getContext(), e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }

                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    AlertDialog.Builder add = new AlertDialog.Builder(getContext());
                    add.setMessage(error.getMessage()).setCancelable(true);
                    AlertDialog alert = add.create();
                    alert.setTitle("Error!!!");
                    alert.show();
                }
            });

            RequestQueue requestQueue = Volley.newRequestQueue(getContext());
            requestQueue.add(jsonArrayRequest);

        }
    }


    void swipRefreshListe() {

        if (SVDepart == "" && SVArrivee == "") {
            afficherAllProposition();
            listeAdapter.notifyDataSetChanged();
            swipeRefresh.setRefreshing(false);
        } else {
            AfficherLesTrajetParVille();
            listeAdapter.notifyDataSetChanged();
            swipeRefresh.setRefreshing(false);
        }

    }


}
