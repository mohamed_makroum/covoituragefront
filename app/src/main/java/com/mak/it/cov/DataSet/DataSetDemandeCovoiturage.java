package com.mak.it.cov.DataSet;

public class DataSetDemandeCovoiturage {

    String idDemande,idProposition,idPassagerFirebase,dateDepart,heureDepart,cout,villeDepart,villeArrivee,validation,nom;

    public String getIdDemande() {
        return idDemande;
    }

    public void setIdDemande(String idDemande) {
        this.idDemande = idDemande;
    }

    public String getIdProposition() {
        return idProposition;
    }

    public void setIdProposition(String idProposition) {
        this.idProposition = idProposition;
    }

    public String getIdPassagerFirebase() {
        return idPassagerFirebase;
    }

    public void setIdPassagerFirebase(String idPassagerFirebase) {
        this.idPassagerFirebase = idPassagerFirebase;
    }

    public String getDateDepart() {
        return dateDepart;
    }

    public void setDateDepart(String dateDepart) {
        this.dateDepart = dateDepart;
    }

    public String getHeureDepart() {
        return heureDepart;
    }

    public void setHeureDepart(String heureDepart) {
        this.heureDepart = heureDepart;
    }

    public String getCout() {
        return cout;
    }

    public void setCout(String cout) {
        this.cout = cout;
    }

    public String getVilleDepart() {
        return villeDepart;
    }

    public void setVilleDepart(String villeDepart) {
        this.villeDepart = villeDepart;
    }

    public String getVilleArrivee() {
        return villeArrivee;
    }

    public void setVilleArrivee(String villeArrivee) {
        this.villeArrivee = villeArrivee;
    }

    public String getValidation() {
        return validation;
    }

    public void setValidation(String validation) {
        this.validation = validation;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
