package com.mak.it.cov.ListeAdapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mak.it.cov.DataSet.DataSetUserVoyagesEnCours;
import com.mak.it.cov.R;

import java.util.ArrayList;

public class ListeAdapterUserVoyagesEnCours extends ArrayAdapter<DataSetUserVoyagesEnCours> {

    private Context con;
    private int res;
    private ArrayList<DataSetUserVoyagesEnCours> arl;


    public ListeAdapterUserVoyagesEnCours(Context context, int resource, ArrayList<DataSetUserVoyagesEnCours> objects) {
        super(context, resource, objects);

        this.con = context;
        this.res = resource;
        this.arl = objects;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = ((Activity) con).getLayoutInflater();
        View view = inflater.inflate(res, parent, false);

        TextView tv_avv_idProposition = (TextView) view.findViewById(R.id.tv_avv_idProposition);
        TextView tv_avv_vd = (TextView) view.findViewById(R.id.tv_avv_vd);
        TextView tv_avv_va = (TextView) view.findViewById(R.id.tv_avv_va);
        TextView tv_avv_dd = (TextView) view.findViewById(R.id.tv_avv_dd);
        TextView tv_avv_nbrDemande = (TextView) view.findViewById(R.id.tv_avv_nd);
        TextView tv_avv_dayLeft = (TextView) view.findViewById(R.id.tv_avv_dayleft);

        DataSetUserVoyagesEnCours userVoyages = arl.get(position);

        tv_avv_idProposition.setText(userVoyages.getIdProposition());
        tv_avv_vd.setText(userVoyages.getVilleDepart());
        tv_avv_va.setText(userVoyages.getVilleArrivee());
        tv_avv_dd.setText(userVoyages.getDateDepart());
        tv_avv_nbrDemande.setText(userVoyages.getNbrDemande());

        if (Integer.valueOf(userVoyages.getDayLeft())==0){

            tv_avv_dayLeft.setText("Aujourd'hui");

        } else if (Integer.valueOf(userVoyages.getDayLeft())>0){

            tv_avv_dayLeft.setText("Passé.");
            tv_avv_dayLeft.setBackgroundColor(502166721);

        } else if (Integer.valueOf(userVoyages.getDayLeft())<0) {

            tv_avv_dayLeft.setText(userVoyages.getDayLeft()+" jours...");
            tv_avv_dayLeft.setBackgroundColor(504612550);

        }



        return view;
    }
}
